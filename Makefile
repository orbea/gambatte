SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

NAME := gambatte
JGNAME := $(NAME)-jg

CFLAGS ?= -O2
CXXFLAGS ?= -O2
FLAGS := -std=c++98 -Wall -Wextra -Wshadow -Wmissing-declarations -pedantic
FLAGS_SOXR := -std=c99
DEPDIR := $(SOURCEDIR)/deps

INCLUDES := -I$(SOURCEDIR)/src -I$(SOURCEDIR)/src/libgambatte
LIBS := -lm -lstdc++

USE_VENDORED_SOXR ?= 0

override ENABLE_SHARED := 0
override ENABLE_STATIC := 0

include $(SOURCEDIR)/jg.mk

CXXSRCS := src/resample/chainresampler.cpp \
	src/resample/i0.cpp \
	src/resample/kaiser50sinc.cpp \
	src/resample/kaiser70sinc.cpp \
	src/resample/makesinckernel.cpp \
	src/resample/resamplerinfo.cpp \
	src/resample/u48div.cpp \
	src/libgambatte/bitmap_font.cpp \
	src/libgambatte/cpu.cpp \
	src/libgambatte/gambatte.cpp \
	src/libgambatte/initstate.cpp \
	src/libgambatte/interrupter.cpp \
	src/libgambatte/interruptrequester.cpp \
	src/libgambatte/loadres.cpp \
	src/libgambatte/memory.cpp \
	src/libgambatte/sound.cpp \
	src/libgambatte/state_osd_elements.cpp \
	src/libgambatte/statesaver.cpp \
	src/libgambatte/tima.cpp \
	src/libgambatte/video.cpp \
	src/libgambatte/file/file.cpp \
	src/libgambatte/mem/cartridge.cpp \
	src/libgambatte/mem/memptrs.cpp \
	src/libgambatte/mem/pakinfo.cpp \
	src/libgambatte/mem/rtc.cpp \
	src/libgambatte/sound/channel1.cpp \
	src/libgambatte/sound/channel2.cpp \
	src/libgambatte/sound/channel3.cpp \
	src/libgambatte/sound/channel4.cpp \
	src/libgambatte/sound/duty_unit.cpp \
	src/libgambatte/sound/envelope_unit.cpp \
	src/libgambatte/sound/length_counter.cpp \
	src/libgambatte/video/ly_counter.cpp \
	src/libgambatte/video/lyc_irq.cpp \
	src/libgambatte/video/next_m0_time.cpp \
	src/libgambatte/video/ppu.cpp \
	src/libgambatte/video/sprite_mapper.cpp \
	jg.cpp

ifneq ($(USE_VENDORED_SOXR), 0)
	CFLAGS_SOXR := -I$(DEPDIR)/soxr
	DEFINES_SOXR := -DSOXR_LIB -DSOXR_VISIBILITY -Dsoxr_EXPORTS
	LIBS_SOXR :=
	CSRCS += soxr/data-io.c \
		soxr/dbesi0.c \
		soxr/fft4g.c \
		soxr/fft4g32.c \
		soxr/fft4g64.c \
		soxr/filter.c \
		soxr/soxr.c \
		soxr/vr32.c
else
	CFLAGS_SOXR := $(shell $(PKG_CONFIG) --cflags soxr)
	LIBS_SOXR := $(shell $(PKG_CONFIG) --libs soxr)
endif

CFLAGS_JG += $(CFLAGS_SOXR)
LIBS += $(LIBS_SOXR)

# Object dirs
MKDIRS := soxr \
	src/resample \
	src/libgambatte/file \
	src/libgambatte/mem \
	src/libgambatte/sound \
	src/libgambatte/video

# List of object files
OBJS := $(patsubst %,$(OBJDIR)/%,$(CSRCS:.c=.o) $(CXXSRCS:.cpp=.o))

# Dependency commands
BUILD_SOXR = $(call COMPILE_C, $(FLAGS_SOXR) $(DEFINES_SOXR))

# Core commands
BUILD_JG = $(call COMPILE_CXX, $(FLAGS) $(INCLUDES) $(CFLAGS_JG))
BUILD_MAIN = $(call COMPILE_CXX, $(FLAGS) $(INCLUDES))

.PHONY: all clean install install-strip uninstall

all: $(TARGET)

# Dep rules
$(OBJDIR)/soxr/%.o: $(DEPDIR)/soxr/%.c $(OBJDIR)/.tag
	$(call COMPILE_INFO,$(BUILD_SOXR))
	@$(BUILD_SOXR)

# Core rules
$(OBJDIR)/src/%.o: $(SOURCEDIR)/src/%.cpp $(OBJDIR)/.tag
	$(call COMPILE_INFO,$(BUILD_MAIN))
	@$(BUILD_MAIN)

# Shim rules
$(OBJDIR)/%.o: $(SOURCEDIR)/%.cpp $(OBJDIR)/.tag
	$(call COMPILE_INFO,$(BUILD_JG))
	@$(BUILD_JG)

$(OBJDIR)/.tag:
	@mkdir -p -- $(patsubst %,$(OBJDIR)/%,$(MKDIRS))
	@touch $@

$(TARGET_MODULE): $(OBJS)
	@mkdir -p $(NAME)
	$(strip $(CXX) -o $@ $^ $(LDFLAGS) $(LIBS) $(SHARED))

$(TARGET_STATIC_JG): $(OBJS)
	@mkdir -p $(NAME)
	$(AR) rcs $@ $^

$(DESKTOP_TARGET): $(SOURCEDIR)/$(DESKTOP)
	@mkdir -p $(NAME)
	@cp $< $@

$(ICONS_TARGET): $(ICONS)
	@mkdir -p $(NAME)/icons
	@cp $(subst $(NAME)/icons,$(SOURCEDIR)/icons,$@) $(NAME)/icons/

$(NAME)/jg-static.mk: $(TARGET_STATIC_JG)
	@printf '%s\n%s\n%s\n%s\n' 'NAME := $(JGNAME)' 'ASSETS :=' \
		'ICONS := $(ICONS_BASE)' 'LIBS_STATIC := $(strip $(LIBS))' > $@

clean:
	rm -rf $(OBJDIR) $(NAME)

ifneq ($(ENABLE_LIB), 0)
install: all
	@mkdir -p $(DESTDIR)$(DOCDIR)
	@mkdir -p $(DESTDIR)$(LIBPATH)
	cp $(TARGET_MODULE) $(DESTDIR)$(LIBPATH)/
	cp $(SOURCEDIR)/README $(DESTDIR)$(DOCDIR)
	cp $(SOURCEDIR)/COPYING $(DESTDIR)$(DOCDIR)
ifneq ($(USE_VENDORED_SOXR), 0)
	cp $(SOURCEDIR)/deps/soxr/LICENSE $(DESTDIR)$(DOCDIR)/LICENSE-soxr
endif

install-strip: install
	strip $(DESTDIR)$(LIBPATH)/$(LIBRARY)
else
install: all
	@echo 'Nothing to install'

install-strip: install
endif

uninstall:
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -f $(DESTDIR)$(LIBPATH)/$(LIBRARY)
